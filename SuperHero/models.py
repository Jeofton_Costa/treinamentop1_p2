from django.db import models

# Create your models here.
#Criando a tabela/classe Superhero

class superHero(models.Model):
    nome = models.CharField(max_length=30)
    first_appear = models.DateField(auto_now_add=False)
    secret_id = models.CharField(max_length=50, null=True)
    #cpf = models.IntegerField(max_length=14, primary_key=True)
    def __str__(self):
        return self.nome

