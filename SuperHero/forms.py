from django.forms import ModelForm
from SuperHero.models import superHero


#Criando formulário para exibição
class RegistroSuperHero(ModelForm):
    class Meta:
        model = superHero
        fields = ['nome','first_appear','secret_id']