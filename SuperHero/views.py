from django.shortcuts import render, redirect, get_object_or_404
from SuperHero.models import superHero
from SuperHero.forms import RegistroSuperHero


# Create your views here.
def create(request):
    form = RegistroSuperHero(request.POST)
    if form.is_valid(): #Verifica se o form está corretamente preenchido, se verdadeiro salva.
        form.save()
        return redirect("list")
    return render(request, 'create.html', {'form':form})

def list(request):
    nome = superHero.objects.all()
    return render(request, 'list.html', {'nome':nome})

def update(request, id):
    nome = get_object_or_404(superHero, pk=id)
    form = RegistroSuperHero(request.POST or None, instance=nome)
    if form.is_valid():#Se as alterações forem válidas a função is_valid, irá salva-las
        form.save()#função salvar
        return redirect("list")
    return render(request,'update.html', {'form':form})

def delete(request, id):
    nome = get_object_or_404(superHero, pk=id)
    if request.method == 'POST':
        nome.delete()
        return redirect('list')
    return render(request, 'delete.html', {'nome': nome})
